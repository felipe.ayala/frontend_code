import { Profesion } from "./profesion";
import { Lenguaje } from "./lenguaje";

export class Encuesta{
    public idEncuesta: number;
    public nombres: String;
    public apellidos: String;
    public edad: number;
    public lugartrabajo: String;
    public profesion: Profesion;
    public lenguaje: Lenguaje;
}