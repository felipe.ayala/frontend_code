export const HOST_BACKEND = `https://rkae4lqxrf.execute-api.us-east-1.amazonaws.com/prod`;
//export const HOST_BACKEND = `http://balanceadorCurso-1902922154.us-east-1.elb.amazonaws.com`;
//export const HOST_BACKEND = `https://hbaeer9xsk.execute-api.us-east-1.amazonaws.com/dev`

export const TIME_UPDATE_GEOLOCALIZATION = 60000;
export const RADIO = 0.029;
export const ZOOM = 16;
export const TOKEN_NAME = "idToken";
export const REFRESH_TOKEN_NAME = "refreshToken";
export const ACCESS_TOKEN_NAME = "accessToken";
export const PARAM_USUARIO = "usuario";