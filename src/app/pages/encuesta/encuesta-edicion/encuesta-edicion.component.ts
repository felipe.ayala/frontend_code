import { ActivatedRoute, Router, Params } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
import { Encuesta } from './../../../_model/encuesta';
import { Component, OnInit } from '@angular/core';
import { EncuestaService } from '../../../_services/encuesta.service';
import { ProfesionService } from '../../../_services/profesion.service';
import { Profesion } from '../../../_model/profesion';
import { LenguajeService } from '../../../_services/lenguaje.service';
import { Lenguaje } from '../../../_model/lenguaje';
import { SecurityService } from '../../../_services/security.service';

@Component({
  selector: 'app-encuesta-edicion',
  templateUrl: './encuesta-edicion.component.html',
  styleUrls: ['./encuesta-edicion.component.css']
})
export class EncuestaEdicionComponent implements OnInit {

  profesiones: Profesion[] = [];
  idProfesionSeleccionado: number;

  lenguajes: Lenguaje[] = [];
  idLenguajeSeleccionado: number;

  id: number;
  encuesta: Encuesta;
  form: FormGroup;
  edicion: boolean = false;
  isAdmin: boolean = false;
  
  constructor(private encuestaService: EncuestaService, private profesionService: ProfesionService,
    private securityService: SecurityService, private lenguajeService: LenguajeService , private route: ActivatedRoute, private router: Router) {
    this.encuesta = new Encuesta();
    this.form = new FormGroup({
      'id': new FormControl(0),
      'nombres': new FormControl(''),
      'apellidos': new FormControl(''),
      'edad': new FormControl(''),
      'lugartrabajo': new FormControl(''),
      'profesion': new FormControl(''),
      'lenguaje': new FormControl('')
    });
  }

  ngOnInit() {
    setTimeout(() => {
      this.isAdmin = this.securityService.esRoleAdmin();
    },1500);

    this.listarProfesiones();
    this.listarLenguajes();
    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
      this.edicion = params['id'] != null;
      this.initForm();
    });
  }

  initForm(){
    if (this.edicion) {
      this.encuestaService.listarEncuestaPorId(this.id).subscribe(data => {
        let id = data.idEncuesta;
        let nombres = data.nombres;
        this.idLenguajeSeleccionado=data.lenguaje.idLenguaje;
        this.idProfesionSeleccionado=data.profesion.idProfesion;
        
        this.form = new FormGroup({
          'id': new FormControl(id),
          'nombres': new FormControl(nombres),
          'apellidos': new FormControl(data.apellidos),
          'edad': new FormControl(data.edad),
          'lugartrabajo': new FormControl(data.lugartrabajo),
          'profesion': new FormControl(data.profesion.idProfesion),
          'lenguaje': new FormControl(data.lenguaje.idLenguaje)
        });
      });
    }
  }

  operar() {
    this.encuesta.idEncuesta = this.form.value['id'];
    this.encuesta.nombres = this.form.value['nombres'];
    this.encuesta.apellidos = this.form.value['apellidos'];
    this.encuesta.edad = this.form.value['edad'];
    this.encuesta.lugartrabajo = this.form.value['lugartrabajo'];
    this.encuesta.profesion = new Profesion();
    this.encuesta.profesion.idProfesion = this.idProfesionSeleccionado;
    this.encuesta.lenguaje = new Lenguaje();
    this.encuesta.lenguaje.idLenguaje = this.idLenguajeSeleccionado;

    if (this.encuesta != null && this.encuesta.idEncuesta > 0) {
      this.encuestaService.modificar(this.encuesta).subscribe(data => {
        this.encuestaService.listar().subscribe(encuesta => {
          this.encuestaService.encuestasCambio.next(encuesta);
          this.encuestaService.mensajeCambio.next("Se modificó");
        });
      });
    } else {
      this.encuestaService.registrar(this.encuesta).subscribe(data => {
        this.encuestaService.listar().subscribe(encuesta => {
          this.encuestaService.encuestasCambio.next(encuesta);
          this.encuestaService.mensajeCambio.next("Se registró");
        });
      });
    }

    this.limpiaformulario();

    this.router.navigate(['/login']);
  }

  limpiaformulario()
  {
    this.form = new FormGroup({
      'id': new FormControl(0),
      'nombres': new FormControl(''),
      'apellidos': new FormControl(''),
      'edad': new FormControl(''),
      'lugartrabajo': new FormControl(''),
      'profesion': new FormControl(''),
      'lenguaje': new FormControl('')
    });

    this.idLenguajeSeleccionado=null;
    this.idProfesionSeleccionado=null;
  }
  listarProfesiones() {
    this.profesionService.listar().subscribe(data => {
      this.profesiones = data;
    });
  }

  listarLenguajes() {
    this.lenguajeService.listar().subscribe(data => {
      this.lenguajes = data;
    });
  }

}
