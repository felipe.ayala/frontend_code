import { ActivatedRoute } from '@angular/router';
import { Encuesta } from './../../_model/encuesta';
import { MatSort, MatPaginator, MatTableDataSource, MatSnackBar } from '@angular/material';
import { Component, OnInit, ViewChild } from '@angular/core';
import { EncuestaService } from '../../_services/encuesta.service';

@Component({
  selector: 'app-encuesta',
  templateUrl: './encuesta.component.html',
  styleUrls: ['./encuesta.component.css']
})

export class EncuestaComponent implements OnInit {

  displayedColumns = ['id', 'nombres', 'apellidos', 'eleccion', 'acciones'];
  dataSource: MatTableDataSource<Encuesta>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  encuestas: Encuesta[] = [];
  
  mensaje: string;

  idEncuestaSeleccionada: number;

  constructor(private encuestaService: EncuestaService, public route: ActivatedRoute, private snackBar: MatSnackBar) { }

  ngOnInit() {    
      this.encuestaService.encuestasCambio.subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });

    this.encuestaService.listar().subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });    
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); 
    filterValue = filterValue.toLowerCase(); 
    this.dataSource.filter = filterValue;
  }
  
  listarEncuesta() {
    this.encuestaService.listar().subscribe(data => {
      this.encuestas = data;
    });
  }
 
  eliminar(idEncuesta: number) {
    this.encuestaService.eliminar(idEncuesta).subscribe(data => {
      this.encuestaService.listar().subscribe(data => {
        this.encuestaService.encuestasCambio.next(data);
        this.encuestaService.mensajeCambio.next('Se eliminó');
      });
    });
  }

  limpiarControles() {
    this.encuestas = [];
    this.mensaje = '';
  }

}
