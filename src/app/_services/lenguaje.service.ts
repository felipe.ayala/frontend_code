import { HttpClient } from '@angular/common/http';
import { HOST_BACKEND } from '../_shared/constants';
import { Subject } from 'rxjs';
import { Lenguaje } from '../_model/lenguaje';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class LenguajeService {
    lenguajeCambio = new Subject<Lenguaje[]>();
    mensajeCambio = new Subject<string>();
    url: string = `${HOST_BACKEND}/api/lenguaje`;
    constructor(private http: HttpClient) { }
    listar() {
        return this.http.get<Lenguaje[]>(this.url);
    }
    listarLenguajePorId(id: number) {
        return this.http.get<Lenguaje>(`${this.url}/${id}`);
    }
    registrar(lenguaje: Lenguaje) {
        return this.http.post(this.url, lenguaje);
    }
    modificar(lenguaje: Lenguaje) {
        return this.http.put(this.url, lenguaje);
    }
    eliminar(id: number) {
        return this.http.delete(`${this.url}/${id}`);
    }
}
