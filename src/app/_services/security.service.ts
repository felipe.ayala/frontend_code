import { Injectable } from '@angular/core';
import { HOST_BACKEND, PARAM_USUARIO, ACCESS_TOKEN_NAME, REFRESH_TOKEN_NAME, TOKEN_NAME } from '../_shared/constants';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { BasicAccess } from '../_model/BasicAccess';

@Injectable({
  providedIn: 'root',
})
export class SecurityService {

  urlOauth: string = `${HOST_BACKEND}/api/security/token`;
  urlLogin: string = `${HOST_BACKEND}/api/security/login`;
  urlRenewPassword: string = `${HOST_BACKEND}/api/security/first-reset-password`;
  urlUpdatePassword: string = `${HOST_BACKEND}/api/security/change-password`;
  urlRefreshToken: string = `${HOST_BACKEND}/api/security/refresh-token`;
  urlSignOut: string = `${HOST_BACKEND}/api/security/signout`;

  constructor(
    private http: HttpClient,
    private router: Router)  { }


  validarToken() {
    return this.http.post(this.urlOauth, "");
  }

  refreshToken(){
    let request = new BasicAccess();
    request.token = sessionStorage.getItem(TOKEN_NAME);
    return this.http.post(this.urlRefreshToken, request);
  }

  cerrarSesion() {
    sessionStorage.clear(); 
    this.router.navigate(['/login']);
  }

  esRoleAdmin(){
    let usuario = JSON.parse(sessionStorage.getItem(PARAM_USUARIO));
    let rpta = false;
    if(usuario != null && usuario.authorities !== null) {
      usuario.authorities.forEach(element => {
        if(element.authority == "ROLE_ADMIN" || element.authority == "ROLE_ADMINISTRADOR"){   
          rpta = true;
        }
      });
    }
    return rpta;
  }
}
