import { HttpClient } from '@angular/common/http';
import { HOST_BACKEND } from '../_shared/constants';
import { Subject } from 'rxjs';
import { Profesion } from '../_model/profesion';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class ProfesionService {
    profesionCambio = new Subject<Profesion[]>();
    mensajeCambio = new Subject<string>();
    url: string = `${HOST_BACKEND}/api/profesion`;
    constructor(private http: HttpClient) { }
    listar() {
        return this.http.get<Profesion[]>(this.url);
    }
    listarProfesionPorId(id: number) {
        return this.http.get<Profesion>(`${this.url}/${id}`);
    }
    registrar(profesion: Profesion) {
        return this.http.post(this.url, profesion);
    }
    modificar(profesion: Profesion) {
        return this.http.put(this.url, profesion);
    }
    eliminar(id: number) {
        return this.http.delete(`${this.url}/${id}`);
    }
}
