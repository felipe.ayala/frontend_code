import { HttpClient } from '@angular/common/http';
import { HOST_BACKEND } from './../_shared/constants';
import { Subject } from 'rxjs';
import { Encuesta } from './../_model/encuesta';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class EncuestaService {
    encuestasCambio = new Subject<Encuesta[]>();
    mensajeCambio = new Subject<string>();
    url: string = `${HOST_BACKEND}/api/encuesta`;
    constructor(private http: HttpClient) { }
    listar() {
        return this.http.get<Encuesta[]>(this.url);
    }
    listarEncuestaPorId(id: number) {
        return this.http.get<Encuesta>(`${this.url}/${id}`);
    }
    registrar(encuesta: Encuesta) {
        return this.http.post(this.url, encuesta);
    }
    modificar(encuesta: Encuesta) {
        return this.http.put(this.url, encuesta);
    }
    eliminar(id: number) {
        return this.http.delete(`${this.url}/${id}`);
    }
}
