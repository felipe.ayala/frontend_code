import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SecurityComponent } from './pages/security/security.component';
import { GuardService } from './_services/guard.service';
import { LogoutComponent } from './pages/logout/logout.component';
import { LoginComponent } from './pages/login/login.component';
import { EncuestaComponent } from './pages/encuesta/encuesta.component';
import { EncuestaEdicionComponent } from './pages/encuesta/encuesta-edicion/encuesta-edicion.component';

const routes: Routes = [
  {
    path: 'encuesta', component: EncuestaComponent, children: [
      { path: 'nuevo', component: EncuestaEdicionComponent },
      { path: 'edicion/:id', component: EncuestaEdicionComponent }
    ], canActivate: [GuardService]
  },
  {path: 'login', component: LoginComponent},
  {path: 'logout', component: LogoutComponent},
  {path: 'security', component: SecurityComponent},  
  {path: '**', redirectTo: 'encuesta', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
